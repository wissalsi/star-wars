# Project name Star Wars
***
Mobile app for Star Wars fans where users can find a list of Star Wars movies and read more details by selecting a specific movie from the main list.
It s a demo application based on modern Android application tech-stacks and architecture.
*****
## Time
I spent 3 days : 1 day in searching and learning
                 2 days in developing
******
## Reference
[TVMaze-Cache](https://github.com/alirezaeiii/TVMaze-Cache)
*******
### Tech stack & Open-source libraries
- Minimum SDK level 21
- [Kotlin](https://kotlinlang.org/) based, [Coroutines](https://github.com/Kotlin/kotlinx.coroutines) for asynchronous.
- [Hilt](https://dagger.dev/hilt/) for dependency injection.
- JetPack
  - Lifecycle - dispose of observing data when lifecycle state changes.
  - ViewModel - UI related data holder, lifecycle aware.
- Architecture
  - MVVM Architecture (View - View Binding - ViewModel - Model)
  - [Data Binding](https://developer.android.com/topic/libraries/data-binding) - Android DataBinding kit for notifying data changes to UI layers.
  - Repository pattern
- [Retrofit & OkHttp3](https://github.com/square/retrofit) - construct the REST APIs and paging network data.
- Memory leak detection library
- [LeakCanary](https://square.github.io/leakcanary/getting_started/) LeakCanary’s knowledge of the internals of the Android Framework gives it a unique ability to narrow down the cause of each leak, helping developers dramatically reduce OutOfMemoryError crashes.

**************
## Licence
 Wissal SLITI
