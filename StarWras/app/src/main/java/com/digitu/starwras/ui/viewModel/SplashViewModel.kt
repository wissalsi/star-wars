package com.digitu.starwras.ui.viewModel

import android.app.Application
import com.digitu.starwras.base.BaseAndroidViewModel
import com.digitu.starwras.global.helper.Navigation
import com.digitu.starwras.global.listener.SchedulerProvider

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

const val SPLASH_TIME = 2000L

@HiltViewModel
class SplashViewModel @Inject constructor(
    application: Application,
    schedulerProvider: SchedulerProvider
):
    BaseAndroidViewModel(application,schedulerProvider)
{
    init {
        checkLoginStatus()
    }

    private fun checkLoginStatus() {
        viewModelScope.launch {
            delay(SPLASH_TIME)
            navigate(Navigation.HomeActivityNavigation)
        }
    }
}
