package com.digitu.starwras.ui.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.digitu.starwras.base.BaseAndroidViewModel
import com.digitu.starwras.data.model.Movie
import com.digitu.starwras.global.listener.SchedulerProvider
import com.digitu.starwras.global.utils.ExtraKeys
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel  @Inject constructor(
  application: Application,
  schedulerProvider: SchedulerProvider,
  savedStateHandle: SavedStateHandle
): BaseAndroidViewModel(application,schedulerProvider) {

  var movie: Movie = savedStateHandle.get<Movie>(ExtraKeys.HomeActivity.HOME_EXTRA_FILM_KEY)!!

  val releaseDate = MutableLiveData(movie.release_date.replace("-","/"))
  val editedDate = MutableLiveData((movie.edited.replace("-","/")).substring(0,10))
  val createdDate = MutableLiveData((movie.created.replace("-","/")).substring(0,10))
  val opening = MutableLiveData(movie.opening_crawl.replace("\r\n"," "))

}

