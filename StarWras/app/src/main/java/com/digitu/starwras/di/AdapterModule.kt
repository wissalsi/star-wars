package com.digitu.starwras.di

import com.digitu.starwras.ui.adapter.MovieAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent


@Module
@InstallIn(ActivityComponent::class)
class ActivityAdapterModule {

    @Provides
    fun provideFilmAdapter(): MovieAdapter{
        return MovieAdapter()
    }
}

