package com.digitu.starwras.di



import com.digitu.starwras.data.repository.MoviesRepository
import com.digitu.starwras.data.repository.MoviesRepositoryImp

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun providMovieRepository(movieImp: MoviesRepositoryImp): MoviesRepository

}
