package com.digitu.starwras.base

import com.digitu.starwras.data.network.APIClient


abstract class BaseRepository(
  protected val apiClient: APIClient,
)
