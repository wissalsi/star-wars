package com.digitu.starwras.data.network

import com.digitu.starwras.data.model.MoviesResponse
import retrofit2.http.GET

interface APIClient {

    @GET("films")
    suspend fun getAllMovies(): MoviesResponse
}
