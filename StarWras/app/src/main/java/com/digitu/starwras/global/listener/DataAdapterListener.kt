package com.digitu.starwras.global.listener

interface DataAdapterListener<T> {
  fun setData(data: T)
}
