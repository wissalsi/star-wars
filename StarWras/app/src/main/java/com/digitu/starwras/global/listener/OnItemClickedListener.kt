package com.digitu.starwras.global.listener

import com.digitu.starwras.data.model.Movie


interface OnItemClickedListener {
    fun onItemClicked(value: Movie)
}
