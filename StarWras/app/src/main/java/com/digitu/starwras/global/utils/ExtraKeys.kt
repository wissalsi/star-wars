package com.digitu.starwras.global.utils

object ExtraKeys {

    class HomeActivity {
        companion object {
            const val HOME_EXTRA_FILM_KEY: String = "home_extra_film"
        }
    }
}
