package com.digitu.starwras.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.digitu.starwras.global.helper.Navigation
import com.digitu.starwras.global.helper.SingleLiveEvent
import com.digitu.starwras.global.listener.SchedulerProvider

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

abstract class BaseAndroidViewModel(
    application: Application,
    protected val schedulerProvider: SchedulerProvider
)  : AndroidViewModel(application) {


    private val job = SupervisorJob()
    protected val viewModelScope = CoroutineScope(job + schedulerProvider.dispatchersUI())
    protected val applicationContext = application.applicationContext!!

    //for navigation events
    private val _navigation: SingleLiveEvent<Navigation> = SingleLiveEvent()
    val navigation: LiveData<Navigation>
        get() = _navigation


    fun navigate(navigationTo: Navigation) {
        _navigation.value = navigationTo
    }

}
