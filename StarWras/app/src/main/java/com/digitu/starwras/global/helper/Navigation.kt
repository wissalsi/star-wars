package com.digitu.starwras.global.helper

import com.digitu.starwras.data.model.Movie



sealed class Navigation {

    object Back : Navigation()

    object HomeActivityNavigation : Navigation()

    data class DetailsActivityNavigation(val movie : Movie) : Navigation()




}
