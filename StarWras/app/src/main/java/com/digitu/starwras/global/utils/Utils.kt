package com.digitu.starwras.global.utils

import android.content.Context
import android.net.ConnectivityManager
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.digitu.starwras.R
import com.digitu.starwras.global.listener.DataAdapterListener
import kotlinx.coroutines.CancellationException

/**
 * ic_status_validated if user is connected to the Internet
 *
 * @param context Context to get resources and device specific display metrics
 * @return A boolean value
 */
fun Context?.isNetworkAvailable(): Boolean {
    if (this == null) return false
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = cm.activeNetworkInfo
    return netInfo != null && netInfo.isConnected
}


/**
 * used for suspend tryCatch
 * @param tryBlock  try block to execute
 * @param catchBlock  catch block to execute
 * @param handleCancellationExceptionManually cancellation exception will manually handled
 *
 */
suspend fun tryCatch(
    tryBlock: suspend () -> Unit,
    catchBlock: suspend (Throwable) -> Unit,
    handleCancellationExceptionManually: Boolean = false
) {
    try {
        tryBlock()
    } catch (e: Throwable) {
        if (e !is CancellationException ||
            handleCancellationExceptionManually
        ) {
            catchBlock(e)
        } else {
            throw e
        }
    }
}




@BindingAdapter("data")
fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, data: T?) {
    data?.let {
        if (recyclerView.adapter is DataAdapterListener<*>) {
            (recyclerView.adapter as DataAdapterListener<T>).setData(it)
        }
    }
}
