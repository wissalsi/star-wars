package com.digitu.starwras.data.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class Movie(
  @Json(name ="title" ) val title: String,
  @Json(name ="episode_id") val episode_id: Int,
  @Json(name ="opening_crawl")  val opening_crawl: String,
  @Json(name ="director") val director: String,
  @Json(name ="producer") val producer: String,
  @Json(name ="release_date") val release_date: String,
  @Json(name ="url") val url: String,
  @Json(name ="created") val created: String,
  @Json(name ="edited" )val edited: String,
): Parcelable {
  constructor(source: Parcel) : this(
  source.readString()!!,
  source.readInt()!!,
  source.readString()!!,
  source.readString()!!,
  source.readString()!!,
  source.readString()!!,
  source.readString()!!,
  source.readString()!!,
  source.readString()!!
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeString(title)
    writeInt(episode_id)
    writeString(opening_crawl)
    writeString(director)
    writeString(producer)
    writeString(release_date)
    writeString(url)
    writeString(created)
    writeString(edited)
  }

  companion object {
    @JvmField
    val CREATOR: Parcelable.Creator<Movie> = object : Parcelable.Creator<Movie> {
      override fun createFromParcel(source: Parcel): Movie = Movie(source)
      override fun newArray(size: Int): Array<Movie?> = arrayOfNulls(size)
    }
  }

}


