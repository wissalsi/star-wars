package com.digitu.starwras.ui.activity.details

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import com.digitu.starwras.R
import com.digitu.starwras.base.BaseActivity
import com.digitu.starwras.databinding.ActivityMovieBinding
import com.digitu.starwras.ui.viewModel.MovieDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieActivity:BaseActivity() {

  private val movieViewModel: MovieDetailsViewModel by viewModels()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val binding = DataBindingUtil.setContentView<ActivityMovieBinding>(this, R.layout.activity_movie)
    val toolbar: Toolbar = findViewById<View>(R.id.movieToolbar) as Toolbar
    toolbar.setNavigationOnClickListener { finish() }
    registerBindingAndBaseObservers(binding)
  }

  private fun registerBindingAndBaseObservers(binding: ActivityMovieBinding) {
    binding.viewModel = movieViewModel
    binding.setLifecycleOwner(this)
    registerBaseObservers(movieViewModel)
  }
}
