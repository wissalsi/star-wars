package com.digitu.starwras.data.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.digitu.starwras.data.model.MoviesResponse


interface MoviesRepository {

    @WorkerThread
    suspend fun getAllMovies(): MoviesResponse
}
