package com.digitu.starwras.data.repository

import androidx.annotation.WorkerThread
import com.digitu.starwras.base.BaseRepository
import com.digitu.starwras.data.model.MoviesResponse
import com.digitu.starwras.data.network.APIClient

import javax.inject.Inject

class MoviesRepositoryImp @Inject constructor(
  apiClient: APIClient,
) : BaseRepository(apiClient), MoviesRepository {


  @WorkerThread
  override suspend fun getAllMovies(): MoviesResponse {
    return apiClient.getAllMovies()
  }
}
