package com.digitu.starwras.ui.adapter


import android.app.Activity
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil


import androidx.recyclerview.widget.RecyclerView
import com.digitu.starwras.data.model.Movie
import com.digitu.starwras.global.listener.DataAdapterListener
import com.digitu.starwras.global.listener.OnItemClickedListener

class MovieAdapter : RecyclerView.Adapter<MovieViewHolder>(),
  DataAdapterListener<List<Movie>> {

  private val movies = arrayListOf<Movie>()
  lateinit var listener: OnItemClickedListener

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
    return MovieViewHolder.create(parent, listener)
  }

  override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
    holder.bind(movies[position])
  }

  override fun getItemCount(): Int {
    return movies.size
  }

  override fun setData(data: List<Movie>) {
    movies.clear()
    movies.addAll(data)
    notifyDataSetChanged()
  }
}

