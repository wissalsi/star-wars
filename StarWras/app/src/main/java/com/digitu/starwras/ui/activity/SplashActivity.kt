package com.digitu.starwras.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.digitu.starwras.R
import com.digitu.starwras.base.BaseActivity
import com.digitu.starwras.databinding.ActivitySplashBinding
import com.digitu.starwras.global.helper.Navigation
import com.digitu.starwras.ui.viewModel.SplashViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity  : BaseActivity() {

    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySplashBinding>(this, R.layout.activity_splash)

        registerBindingAndBaseObservers(binding)
    }

    override fun navigate(navigationTo: Navigation) {
        when (navigationTo) {
            is Navigation.HomeActivityNavigation -> {
                navigateToHome()
            }
        }
    }

    private fun navigateToHome() {
        Intent(this, MoviesListActivity::class.java).let {
            startActivity(it)
            finish()
        }
    }

    private fun registerBindingAndBaseObservers(binding: ActivitySplashBinding) {
        binding.viewModel = viewModel
        binding.setLifecycleOwner(this)
        registerBaseObservers(viewModel)
    }
}
