package com.digitu.starwras.data.model

import com.squareup.moshi.Json

data class MoviesResponse(
    @Json(name = "results")
    val films: List<Movie>? = emptyList(),
    @Json(name = "count")
    val count: Int

)
