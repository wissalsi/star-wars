package com.digitu.starwras.data.network

import android.content.Context
import com.digitu.starwras.global.utils.isNetworkAvailable
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


class EndpointInterceptor(private val context: Context) :
    Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if (context.isNetworkAvailable()) {
                request = request.newBuilder()
                    .method(request.method, request.body)
                    .build()

        } else {
            throw NetworkNotFoundException()
        }
        val response = chain.proceed(request)
        return response
    }

    class NetworkNotFoundException : IOException("No network available")

}
