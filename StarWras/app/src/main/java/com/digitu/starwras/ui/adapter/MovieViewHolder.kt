package com.digitu.starwras.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.digitu.starwras.data.model.Movie
import com.digitu.starwras.databinding.MoviesListItemsBinding
import com.digitu.starwras.global.listener.OnItemClickedListener

class MovieViewHolder (private val binding: MoviesListItemsBinding,
                       private val onItemClickedListener: OnItemClickedListener,
) : RecyclerView.ViewHolder(binding.root) {

  fun bind(film: Movie) {
    binding.item = film
    binding.title = film.title
    binding.date = film.release_date.replace("-","/")
    binding.director = film.director
    binding.producer = film.producer
    binding.opening = film.opening_crawl.replace("\r\n"," ")
    binding.onItemClickedListener = onItemClickedListener
  }

  companion object {
    fun create(parent: ViewGroup,onItemClickedListener: OnItemClickedListener): MovieViewHolder {
      val inflater = LayoutInflater.from(parent.context)
      val binding = MoviesListItemsBinding.inflate(inflater, parent, false)
      return MovieViewHolder(
        binding,
        onItemClickedListener
      )
    }
  }
}
