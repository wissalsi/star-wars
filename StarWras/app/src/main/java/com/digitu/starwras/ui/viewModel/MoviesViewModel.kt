package com.digitu.starwras.ui.viewModel

import android.app.Application
import android.content.ContentValues
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.digitu.starwras.R
import com.digitu.starwras.base.BaseAndroidViewModel
import com.digitu.starwras.data.model.Movie
import com.digitu.starwras.data.repository.MoviesRepository
import com.digitu.starwras.global.helper.FetchState
import com.digitu.starwras.global.helper.Navigation
import com.digitu.starwras.global.helper.RefreshState
import com.digitu.starwras.global.listener.OnItemClickedListener
import com.digitu.starwras.global.listener.SchedulerProvider
import com.digitu.starwras.global.utils.DebugLog
import com.digitu.starwras.global.utils.tryCatch
import com.digitu.starwras.data.network.EndpointInterceptor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
  application: Application,
  schedulerProvider: SchedulerProvider,
  private val moviesRepository: MoviesRepository):
  BaseAndroidViewModel(application,schedulerProvider),
  OnItemClickedListener {

  var fetchState: MutableLiveData<FetchState> = MutableLiveData(FetchState.Fetching)
  var refreshState: MutableLiveData<RefreshState> = MutableLiveData()
  val moviesList = MutableLiveData<List<Movie>>()
  val count = MutableLiveData<String>()


  init {
    loadInitial()
  }

  var errorMessage = fetchState.map {
    if (it is FetchState.FetchError) {
      when (it.throwable) {
        is EndpointInterceptor.NetworkNotFoundException -> {
          applicationContext.getString(R.string.global_error_unavailable_network)
        }
        is HttpException -> {
          when (it.throwable.code()) {
            //other default handler
            else -> applicationContext.getString(R.string.global_error_server)
          }
        }
        else -> {
          applicationContext.getString(R.string.global_error_server)
        }
      }
    } else {
      ""
    }
  }

  var isRefreshing = refreshState.map {
    it == RefreshState.Refreshing
  }

  var isLoading = fetchState.map {
    it == FetchState.Fetching
  }

  var isError = errorMessage.map {
    it.isBlank().not()
  }

  private fun updateFetchState(fetchState: FetchState) {
    this.fetchState.value = fetchState
  }

  private fun updateRefreshState(refreshState: RefreshState) {
    this.refreshState.value = refreshState
  }

  fun loadInitial() {
    updateFetchState(FetchState.Fetching)
    viewModelScope.launch {
      tryCatch({
        val response = withContext(schedulerProvider.dispatchersIO()) {
        moviesRepository.getAllMovies()
        }
        moviesList.value = response.films!!
        count.value = "Total " + response.count + " Mvoies"
        updateFetchState(FetchState.FetchDone(response.films.isNullOrEmpty()))
      }, {
        DebugLog.d(ContentValues.TAG, it.toString())
        updateFetchState(FetchState.FetchError(it))
      })
    }
  }


  fun onRefresh(){
    if (refreshState.value == RefreshState.Refreshing || (fetchState.value == FetchState.Fetching)) {
      return
    }
    updateRefreshState(RefreshState.Refreshing)
    viewModelScope.launch {
      tryCatch({
        val response = withContext(schedulerProvider.dispatchersIO()) {
          moviesRepository.getAllMovies()
        }
        moviesList.value = response.films!!
        count.value = "Total " +response.count+" Movies"
        updateRefreshState(RefreshState.RefreshingDone)
      }, {
        DebugLog.d(ContentValues.TAG, it.toString())
        updateRefreshState(RefreshState.RefreshingError(it))
      })
    }
  }


  fun onRetry(){
    loadInitial()
  }
  override fun onItemClicked(value: Movie) {
    navigate(Navigation.DetailsActivityNavigation(value))
  }
}
