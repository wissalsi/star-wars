package com.digitu.starwras

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
open class SWApplication: Application() {

    companion object {

        private lateinit var instance: SWApplication

        fun getInstance(): SWApplication = instance
    }
}
