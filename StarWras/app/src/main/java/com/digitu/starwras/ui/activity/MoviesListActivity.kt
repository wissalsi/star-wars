package com.digitu.starwras.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digitu.starwras.R
import com.digitu.starwras.base.BaseActivity
import com.digitu.starwras.data.model.Movie
import com.digitu.starwras.databinding.ActivityMovieListBinding
import com.digitu.starwras.global.helper.Navigation
import com.digitu.starwras.global.listener.OnItemClickedListener
import com.digitu.starwras.global.utils.ExtraKeys
import com.digitu.starwras.ui.activity.details.MovieActivity
import com.digitu.starwras.ui.adapter.MovieAdapter

import com.digitu.starwras.ui.viewModel.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoviesListActivity : BaseActivity() {


  @Inject
  lateinit var movieListAdapter: MovieAdapter

  private val viewModel: MoviesViewModel by viewModels()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val binding = DataBindingUtil.setContentView<ActivityMovieListBinding>(this, R.layout.activity_movie_list)

    registerBindingAndBaseObservers(binding)

  }


  override fun navigate(navigationTo: Navigation) {
    when (navigationTo) {
      is Navigation.DetailsActivityNavigation -> {
        navigateToDetails(navigationTo.movie)
      }
    }
  }


  private fun navigateToDetails(movie : Movie) {
    Intent(this, MovieActivity::class.java).let {
      it.putExtra(ExtraKeys.HomeActivity.HOME_EXTRA_FILM_KEY, movie)
      startActivity(it)
    }
  }


  private fun registerRecycler(binding: ActivityMovieListBinding) {
    movieListAdapter.listener = viewModel as (OnItemClickedListener)
    binding.recyclerMovies.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    binding.recyclerMovies.adapter = movieListAdapter
  }


  private fun registerBindingAndBaseObservers(binding: ActivityMovieListBinding) {
    binding.viewModel = viewModel
    binding.setLifecycleOwner(this)
    registerBaseObservers(viewModel)
    registerRecycler(binding)
  }
}
