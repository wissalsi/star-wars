package com.digitu.starwras.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.digitu.starwras.global.helper.Navigation
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity() {



    protected fun registerBaseObservers(viewModel: ViewModel) {
        if (viewModel is BaseAndroidViewModel) {
            registerNavigation(viewModel)
        }
    }

    private fun registerNavigation(viewModel: BaseAndroidViewModel) {
        viewModel.navigation.observe(this, Observer { navigate(it) })
    }



    open fun navigate(navigationTo: Navigation) {

    }


}
